import { useRouter } from 'next/router';
import { useCallback, useState } from 'react';

export default function App() {
  const router = useRouter()

  const [inputId, setInputId] = useState('')

  const handleSubmit = useCallback((e) => {
    e.preventDefault()    
    router.push(`/danfes/${inputId}`)
  }, [router, inputId]) 
  
  return(
    <div>
    <form onSubmit={handleSubmit}>    
      <label htmlFor="Id">Id</label>
      <input onChange={e => setInputId(e.target.value)} type="text" id="Id" name="Id" value={inputId} />
      <button type="submit">Enviar Id!</button>
    </form>
    </div>
  )
}
