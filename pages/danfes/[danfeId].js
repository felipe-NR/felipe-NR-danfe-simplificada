  //TODO add a loading spinner

import Danfe from 'danfe-simplificada';
import fetch from "node-fetch";

export default function App({data}) {

    return <div className='wrapper' dangerouslySetInnerHTML={{ __html: data }} />  
  
}

export async function getServerSideProps(context) {   
    
    async function fetchXml() {
      const danfeId = context.params.danfeId 
      const danfeLogoURL = process.env.danfeLogoURL

      const data = await fetch(`https://drive.google.com/u/0/uc?id=${danfeId}&export=download&confirm=download%20anyway`)
      const xml = await data.text()
      const danfe = Danfe.fromXML(xml, danfeLogoURL)
  
      const result = danfe.toHtml()
  
      if(result.error?.length > 0) {
        return result.error
      }else return result
    }
    
    return {
      props: {
        data: await fetchXml()
      }
    }    
}
