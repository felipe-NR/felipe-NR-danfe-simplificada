/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
}

module.exports = nextConfig

module.exports = {
  env: {
    danfeLogoURL: 'https://www.nicepng.com/png/full/430-4309438_nota-fiscal-eletronica-icon-nota-fiscal-eletronica-png.png',
    URL: {
      local: 'http://localhost:3000/',
      prod: 'https://felipe-nr-danfe-simplificada.vercel.app/'
    }
  },
}
