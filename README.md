<h1 align="center" >
  <p>
    Gerenciador de horas<br>
    Next.js - MongoDB <br>
  </p>
</h1>

## 📕 Sobre

O projeto tem como objetivo construir uma aplicação de uso pessoal para controle e lançamento de horas.

## 🔗 Acesso a aplicação

<p align="center">
<a href="">
<img src="" width="500" alt="Imagem da aplicação ?"/><br>
Acesso à aplicação
</a>
</p>


## 📝 Funcionalidades
- Adicionar novo lançamento de janela de horas ✔️

## 🔧 Ferramentas Utilizadas

- [Next.js](https://nextjs.org/)
- [MongoDB](https://www.mongodb.com/)


## Autor

| ![Felipe Neves Ricardo](https://avatars.githubusercontent.com/u/74023958?v=4&s=150)|
|:---------------------:|
|  [Felipe Neves Ricardo](https://github.com/felipe-NR/)   |